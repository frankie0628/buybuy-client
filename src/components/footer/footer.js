import React from 'react'
import './footer.scss'

const Footer = props => (
  <footer className='d-flex justify-content-center align-items-center'>
    <div>
      <i className='fa fa-copyright' />
      <span> Franklin Reserves All Right.</span>
    </div>
  </footer>
)

export default Footer
