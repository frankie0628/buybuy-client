export default {

  'signup'         : '/authenticate/signup',
  'product_search' : '/product',
  'product_details': '/product/:id',

  // Require user login
  'user'                 : '/api/user',
  'user_validation'      : '/api/user/:uid/validate',
  'user_product'         : '/api/product',
  'user_product_details' : '/api/product/:id',
  'image'                : '/api/image'
}
