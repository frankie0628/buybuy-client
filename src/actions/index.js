import User from './user.js'
import Product from './product.js'

export {
  Product,
  User
}
